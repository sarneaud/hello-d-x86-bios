module hello;
@nogc:
nothrow:

extern(C) void systemEntry()
{
	foreach (char c; "Hello from D\n")
	{
		putChar(c);
	}
}

enum kCom1DataPort = 0x3f8;
enum kCom1LineStatusPort = 0x3fd;

void putChar(char c)
{
	ubyte line_status;
	do
	{
		line_status = inPortByte(kCom1LineStatusPort);
	} while (!(line_status & (1 << 5)));  // Wait for transmit buffer empty flag

	outPortByte(kCom1DataPort, cast(ubyte) c);
}

void outPortByte(ushort port, ubyte b)
{
	asm @nogc nothrow
	{
		mov DX, port;
		mov AL, b;
		out DX, AL;
	}
}

ubyte inPortByte(ushort port)
{
	ubyte result;
	asm @nogc nothrow
	{
		mov DX, port;
		in AL, DX;
		mov result, AL;
	}
	return result;
}
