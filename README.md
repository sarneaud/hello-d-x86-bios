# Hello D x86 BIOS Bootloader Example

A "hello world" example x86 bootloader.  Unlike most "hello world" bootloaders, it's complete enough to boot to a high level systems language, in this case [D](https://dlang.org/).

[The boot code is explained on my blog.](https://theartofmachinery.com/2017/01/24/boot_to_d.html)

## Caveats

I wondered if it were possible to boot directly to a large payload in a high-level language using a single 512B bootloader.  The answer turns out to be yes, but it required a lot of compromises, mostly in portability.  *THIS BOOTLOADER PROBABLY DOESN'T WORK ON YOUR MACHINE.*  It works on qemu and bochs (but not with floppy disks or USB drives), which is good enough for a demo.

This is highly experimental work and [requires some hacks](https://theartofmachinery.com/2016/12/18/d_without_runtime.html) that aren't officially supported by the core D developers.

## Prerequisites

The example uses

* NASM
* DMD
* qemu
* ld
* make

The last two should be on most good \*nix systems.

I developed and tested the code using DMD 2.071.0 on GNU/Linux.  It should be possible to use other D compilers, perhaps with some tweaking.  It'll probably take a little more effort to make it work on other operating systems.

Look at the `Makefile` for build targets.  If all goes well, `make qemu` should run the demo.

## "It doesn't work for me"

Check the caveats.

If you get linker errors, try reading [that post about linker hacking](https://theartofmachinery.com/2016/12/18/d_without_runtime.html).  One exception is if you get linker errors about `_GLOBAL_OFFSET_TABLE_`.  This is for position independent code (PIC), which you don't want in this bare-metal code.  It affects code generation itself, and can't be undone at the linker level.  PIC was turned on globally in new versions of DMD, and the only way to turn it off (as of January 2017) is to copy your `dmd.conf` (should be in your DMD install tree) to the directory where you're building this bootloader, and edit out the `-fPIC`.
