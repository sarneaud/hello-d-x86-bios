DMD=dmd
QEMU=qemu-system-x86_64

hello-unstripped.o: hello.d
	${DMD} -c -boundscheck=off -release -betterC -debuglib= -defaultlib= -m32 -of$@ hello.d

hello.o: hello-unstripped.o
	objcopy --wildcard -R '*.eh_frame' -R '*.d_dso_*' -R '*.__dmd_personality_v0' --strip-unneeded $< $@

boot.o: boot.asm image.ld
	nasm -f elf32 -o boot.o boot.asm

image.bin: hello.o boot.o image.ld
	ld -T image.ld -o image.bin --gc-sections -Map=image.map -m elf_i386 hello.o boot.o

qemu: image.bin
	${QEMU} -serial stdio -drive file=image.bin,format=raw

clean:
	rm -f image.bin
	rm -f image.map
	rm -f *.o

.PHONY: clean qemu
